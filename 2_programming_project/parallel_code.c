#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

int perform_op(char *op, long long int a, long long int b) {
    if (strcmp("+", op) == 0)
        return a+b;

    else if (strcmp("max", op) == 0)
        return (a > b) ? a : b;
    
    else if (strcmp("min", op) == 0)
        return (a < b) ? a : b;

    else {
        fprintf(stderr, "shouldn't get here.....\n");
        printf("%s", op);
        exit(1);
    }
}

void MyReduce(int rank, int size, long int local_sum, int num_elements_per_proc, char *op) {
    long long int global_sum = local_sum, sum_t;
    int partner, t;
    MPI_Status status;

    for (t = 0; t < ceil(log2(size)); t++) {
        partner = rank ^ (1 << t);

        MPI_Sendrecv(&global_sum, 1, MPI_LONG_LONG_INT, partner, 0, &sum_t, 1, MPI_LONG, partner, 0, \
                        MPI_COMM_WORLD, &status);
        global_sum = perform_op(op, sum_t, global_sum);
    }

    if (rank == 0)
        printf("Total sum = %lld\n", global_sum);
}

void NaiveReduce(int rank, int size, long int local_sum, int num_elements_per_proc, char *op) {
    long long int global_sum, recv, send;
    MPI_Status status;

    if (size > 1) {
        //calculate sum
        if (rank < size-1) {
            MPI_Recv(&recv, 1, MPI_LONG_LONG_INT, rank+1, 0, MPI_COMM_WORLD, &status);
            send = perform_op(op, recv, local_sum);
            if (rank > 0)
                MPI_Send(&send, 1, MPI_LONG_LONG_INT, rank-1, 0, MPI_COMM_WORLD);
        }
        else
            MPI_Send(&local_sum, 1, MPI_LONG_LONG_INT, rank-1, 0, MPI_COMM_WORLD);

        //broadcast sum wihtout "broadcasting"
        if (rank > 0) {
            MPI_Recv(&global_sum, 1, MPI_LONG_LONG_INT, rank-1, 0, MPI_COMM_WORLD, &status);
            if (rank < size-1)
                MPI_Send(&global_sum, 1, MPI_LONG_LONG_INT, rank+1, 0, MPI_COMM_WORLD);
        }
        else {
            global_sum = send;
            MPI_Send(&global_sum, 1, MPI_LONG_LONG_INT, rank+1, 0, MPI_COMM_WORLD);
        }
    }
    
    if (rank == size-1)
        printf("Total sum at the last process is: %lld\n", global_sum);
}

void MPILibraryReduce(int rank, int size, long int local_sum, \
                        int num_elements_per_proc, char *op) {
    long long int global_sum = 0;

    if (strcmp("+", op) == 0)
        MPI_Allreduce(&local_sum, &global_sum, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);
    
    else if (strcmp("max", op) == 0)
        MPI_Allreduce(&local_sum, &global_sum, 1, MPI_LONG_LONG_INT, MPI_MAX, MPI_COMM_WORLD);
    
    else if (strcmp("min", op) == 0)
        MPI_Allreduce(&local_sum, &global_sum, 1, MPI_LONG_LONG_INT, MPI_MIN, MPI_COMM_WORLD);
    
    else {
        fprintf(stderr, "Shouldn't get here...\n");
        exit(1);
    }

    if (rank == 0)
        printf("Total sum = %lld\n", global_sum);
}

long int *generate_random_numbers(int num_elements_per_proc, int rank) {
    int i;

    long int *rand_nums = (long int *)malloc(sizeof(long int) * num_elements_per_proc);
    assert(rand_nums != NULL);

    srand(rank);
    for (i = 0; i < num_elements_per_proc; i++)
        rand_nums[i] = (long int)(rand() % 1000);

    return rand_nums;
}

int main(int argc, char** argv) {
    int rank, size, i;

    if (argc != 4) {
        fprintf(stderr, "Usage: <code_name> <n> <1,2,3> 1: MyReduce, \
                        2: NaiveReduce and 3: MPILibraryReduce <+,max,min>\n");
        exit(1);
    }

    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int num_elements = atoi(argv[1]);
    int which_func = atoi(argv[2]);
    char *op = argv[3];

    if ((strcmp(op,"+") != 0) && (strcmp(op,"max") != 0)&& (strcmp(op,"min") != 0)) {
        fprintf(stderr, "Only '+', 'max' and 'min' operators are allowed..");
        exit(1);
    }

    int num_elements_per_proc = num_elements / size;
    long int *rand_nums = NULL;
    rand_nums = generate_random_numbers(num_elements_per_proc, rank);

    long int local_sum = 0; 
    struct timeval start_time, end_time;

    gettimeofday(&start_time, NULL);
    for (i = 0; i < num_elements_per_proc; i++)                                         //Computing Local operation
        local_sum = perform_op(op, rand_nums[i], local_sum);

    if (which_func == 3)
        MPILibraryReduce(rank, size, local_sum, num_elements_per_proc, op);
    
    else if (which_func == 2)
        NaiveReduce(rank, size, local_sum, num_elements_per_proc, op);
    
    else if (which_func == 1)
        MyReduce(rank, size, local_sum, num_elements_per_proc, op);
    
    else {
        fprintf(stderr, "Shouldn't reach here..");
        exit(1);
    }
    gettimeofday(&end_time, NULL);

    int time_taken = (end_time.tv_sec * 1000000 + end_time.tv_usec) -
                                (start_time.tv_sec * 1000000 + start_time.tv_usec);

    printf("Process %d, with operation %s and local sum of %ld finised in time = %d us\n", rank, op, local_sum, time_taken);

    free(rand_nums);

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
}