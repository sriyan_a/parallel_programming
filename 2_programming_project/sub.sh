#!/bin/bash

for((a = 3; a >= 1; a--))
do
    for((op = 1; op <= 2; op++))
    do
        if [ "$op" -eq 1 ];
        then 
            oper='+'
        elif [ "$op" -eq 2 ]
        then 
            oper="max"
        else 
            echo 'should not happen'
        fi

        echo '##### Values for operation: '$oper' and algorihtm: '$a' ######'
        for((j = 1000; j <= $((10**8)); j = j * 2))
        do
            declare -a arr
            declare -i c=1
            for((i = 1; i <= 64; i = i * 2))
            do
                start=`date +%s%N`
                mpirun --mca orte_base_help_aggregate 0 -np $i parallel_sum $j $a $oper > ./output_${i}/out_${j}_${a}_${oper}.txt
                end=`date +%s%N`
                arr[$c]=$(((end-start)/1000))
                ((c++))
            done
            printf "%s,%s,%s,%s,%s,%s,%s\n" "${arr[@]:1}"
        done
    done
done