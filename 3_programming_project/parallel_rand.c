#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

#ifndef M_H
#define M_H
typedef struct matrix {
	unsigned int M[2][2];
} myMat;
#endif

unsigned int seed;
int A, B;
myMat m_0 = {{{1,0},{0,1}}};

myMat multiplyMatrix(myMat m_1, myMat m_2, int P) {
	myMat m_3;
	
	m_3.M[0][0] = ((m_1.M[0][0] * m_2.M[0][0]) + (m_1.M[0][1] * m_2.M[1][0])) % P;
	m_3.M[0][1] = ((m_1.M[0][0] * m_2.M[0][1]) + (m_1.M[0][1] * m_2.M[1][1])) % P;
	m_3.M[1][0] = ((m_1.M[1][0] * m_2.M[0][0]) + (m_1.M[1][1] * m_2.M[1][0])) % P;
	m_3.M[1][1] = ((m_1.M[1][0] * m_2.M[0][1]) + (m_1.M[1][1] * m_2.M[1][1])) % P;

	return m_3;
}

myMat parallelPrefix(myMat m_local, myMat m_offset, int p, int rank, int P) {	
	int mask, i, partner;
	int recv[2];

	for (i=0; i<ceil(log2(p)); i++) {
		partner = rank^(1 << i);
		
		int send[2] = {m_local.M[0][0], m_local.M[1][0]};
		MPI_Sendrecv(&send, 2, MPI_INT, partner, 0, &recv, 2, MPI_INT, partner, 0, \
                        MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		myMat m_temp = {{{recv[0], 0}, {recv[1], 0}}};

		m_local = multiplyMatrix(m_local, m_temp, P);
		
		if (rank > partner)
			m_offset = multiplyMatrix(m_offset, m_temp, P);
	}
	return m_offset;
}

double parallelRandGen(int *gen_array, myMat M, int n, int P, int rank, int p) {
	int i;
	myMat m_local = m_0, m_temp = m_0;
	myMat m_offset = m_0;
	myMat mul_m = {{{seed, 1},{0, 0}}};
	// myMat* x_loc = malloc(n * sizeof(myMat));
	myMat x_loc[n];
	
	for (i=rank*n/p; i<((rank+1)*n/p); i++) {
		x_loc[i] = m_local;
		m_local = multiplyMatrix(m_local, M, P);
	}
	
	m_offset = parallelPrefix(m_local, m_offset, p, rank, P); 
	
	for (i=rank*n/p; i<((rank+1)*n/p); i++) {
		x_loc[i] = multiplyMatrix(x_loc[i], m_offset, P);
	
		m_temp = multiplyMatrix(mul_m, x_loc[i], P);
		gen_array[i] = m_temp.M[0][0];
	}
}

void runParallelMatrix(int *gen_array, myMat M, int P, int rank, int p, int N) {
	int i;
	time_t start_t, end_t;
	double run_time_loc = 0, run_times[p], run_max;
	FILE *fp;
	int n;
	
	for (n=128; n<=N; n*=2) {	
		MPI_Barrier(MPI_COMM_WORLD);

		for (i=0; i<10; i++) {
			start_t = clock();
			parallelRandGen(gen_array, M, n, P, rank, p);
			end_t = clock();
			run_time_loc += (double)(end_t - start_t) / CLOCKS_PER_SEC;
		}

		run_time_loc /= 10;
		MPI_Gather(&run_time_loc, 1, MPI_DOUBLE, run_times, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		
		if (rank == 0) {
			for (i=0; i<p; i++) {
				if (run_times[i] > run_max)
					run_max = run_times[i];
			}
			fp = fopen("max_runtimes.txt", "a");
			fprintf(fp, "%lf\n", run_max);
			fclose(fp);
		}
		run_time_loc = 0;
		run_max = 0;
	}
}

void serialBaseline(int *gen_array, int P, int N) {
	int i;

	gen_array[0] = seed;
	for (i = 1; i < N; i++) {
		gen_array[i] = (A*gen_array[i-1] + B) % P;
	}
}

void serialMatrix(int *gen_array, myMat M, int P, int N) {
	int i;
	myMat m_next = M;
	myMat mul_m = {{{seed, 1},{0, 0}}}, m_temp;

	gen_array[0] = seed;
	for (i=1; i<N; i++) {
		m_temp = multiplyMatrix(mul_m, m_next, P);
		gen_array[i] = m_temp.M[0][0];
		m_next = multiplyMatrix(m_next, M, P);
	}
}

int main(int argc, char *argv[]) {
	int rank, p, P, N, i, j, n;
	unsigned int temp_seed_arr[1];
	time_t start_t, end_t;
	double run_time_loc = 0;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);

	A = atoi(argv[1]);
	B = atoi(argv[2]);
	P = atoi(argv[3]);
	N = pow(2, atoi(argv[4]));
	myMat M = {{{A, 0}, {B, 1}}};

	if (rank == 0) {
		srand(10);
		temp_seed_arr[0] = rand();
	}
	MPI_Bcast(temp_seed_arr, 1, MPI_INT, 0, MPI_COMM_WORLD);
	if (temp_seed_arr[0] > P) seed = temp_seed_arr[0] % P;

	int *gen_array = malloc(N * sizeof(int));
	//***************Running Parallel Matrix Meth and collecting data**********************
	runParallelMatrix(gen_array, M, P, rank, p, N);

	/*
	//*************Only to print the values******************
	for (i = 0; i < p; i++) {
		if (rank == i) {
			printf("rank: %d\n", i);
			for (j=rank*(N/p); j<(rank+1)*(N/p); j++) {
				printf("%d\n", gen_array[j]);
			}
		}
	}

	//********Use the following for correctness check***********
	if(rank == 0) {
		printf("serial:\n");
		for (n=128; n<=N; n*=2) {
			start_t = clock();
			serialBaseline(gen_array, P, n);
			end_t = clock();
			run_time_loc += (double)(end_t - start_t) / CLOCKS_PER_SEC;
			printf("%lf\n", run_time_loc);
			for (i=0; i<N; i++) {
				printf("%d\n", gen_array[i]);
			}
		}
		printf("serial matrix:\n");
		for (n=128; n<=N; n*=2) {
			start_t = clock();
			serialMatrix(gen_array, M, P, n);
			end_t = clock();
			run_time_loc += (double)(end_t - start_t) / CLOCKS_PER_SEC;
			printf("%lf\n", run_time_loc);
			for (i=0; i<N; i++) {
				printf("%d\n", gen_array[i]);
			}
		}
	}
	*/
	
	MPI_Finalize();
	return 0;
}
