#include "PR.h"

int main(int argc, char * argv[]) {
    int k, p, i;
    double d;
    bool flags[3] = {false, false, false};

    if(argc <= 4) {
        cout << "Format is:\n./rank <file_name> <thread_count> <walk_length> <damping_ratio>\n";
        exit(0);
    }

    p = atoi(argv[2]);
    k = atoi(argv[3]);
    d = atof(argv[4]);
    omp_set_num_threads(p);

    PR::run(argv[1], k, d);

    return 0;
}
