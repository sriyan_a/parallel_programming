#include "PR.h"

AL::AL() {
    a_list = vector< vector <unsigned int> >(0);
}

AL::AL(fstream &adj_steam) {
    char buf[256];
    int i;
    unsigned int node_count, current_node, adj;
    string line;
    char * t;

    do {
        adj_steam.getline(buf, 256);
    } while(strncmp("# Nodes: ", buf, 9) != 0);

    t = strtok(buf + 9, " \t");
    node_count = atoi(t);

    while(buf[0] == '#')
        adj_steam.getline(buf, 256);

    a_list = vector <vector <unsigned int> >(node_count, vector<unsigned int>(0));

    while(!adj_steam.eof()) {
        if (node_count == 4039)
            t = strtok(buf, " ");
        else
            t = strtok(buf, "\t");
        current_node = atoi(t);
        if(current_node > a_list.size())
        a_list.resize(current_node);

        t = strtok(NULL, "\n");
        adj = atoi(t);
        if(adj > a_list.size())
        a_list.resize(adj);
        
        a_list[current_node].push_back(atoi(t));
        adj_steam.getline(buf, 256);
    }
}

unsigned int AL::Count(void) const {
    return a_list.size();
}

vector<unsigned int> AL::Edges(const unsigned int node) const {
    if(node < a_list.size())
        return a_list.at(node);
    else 
        return vector<unsigned int>();
}