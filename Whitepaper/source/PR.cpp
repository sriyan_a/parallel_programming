#include "PR.h"

AL PR::adj_list = AL();
unsigned int PR::walk_len = 1;
unsigned int PR::rank_list[1000000];
double PR::damping_ratio = 0.0;
double PR::rank_time = 0.0;

int PR::run(const char * fname, unsigned int k, double d) {
    fstream in_file;
    unsigned int i = 0, j;
    unsigned int top5[5] = {0, 0, 0, 0, 0}, top_5_idx[5] = {0, 0, 0, 0, 0};

    in_file.open(fname);

    adj_list = AL(in_file);
    walk_len = k;
    damping_ratio = d;

    rank_time = omp_get_wtime();
    PR::Rank();
    rank_time = omp_get_wtime() - rank_time;

    // for(i = 0; i < adj_list.Count(); i++) {
    //     for(j = 0; j < 5; j++) {
    //         if(rank_list[i] < top5[j])
    //             break;
    //         else {
    //             if(j > 0) {
    //                 top5[j - 1] = top5[j];
    //                 top_5_idx[j - 1] = top_5_idx[j];
    //             }
    //             top5[j] = rank_list[i];
    //             top_5_idx[j] = i;
    //         }
    //     }
    // }

    // cout << "top5:" << endl;
    // for (i=0; i < 5; i++) {
    //     cout << 5-i << ". value: " << top5[i] << " node: " << top_5_idx[i] << endl;
    // }

    cout << rank_time << endl;
}

int PR::Rank() {
    unsigned int i, j, node_cnt, next_node;
    vector<unsigned int> curr_node;
    double random;
    struct drand48_data r_buff;

    node_cnt = adj_list.Count();
    // cout << "node count:" << node_cnt << endl;
    memset(rank_list, 0, 1000000);

    srand48_r((unsigned int)time(0), &r_buff);
    #pragma omp parallel for schedule(static) private(curr_node, next_node, r_buff, random)
    for(i = 0; i < node_cnt; i++ ) {
        curr_node = adj_list.Edges(i);
        for(int j = 0; j < walk_len; j++) {
            drand48_r(&r_buff, &random);
            
            if(curr_node.size() == 0 || random <= damping_ratio) {
                drand48_r(&r_buff, &random);
                next_node = (unsigned int)(random * node_cnt);
            }
            else {
                drand48_r(&r_buff, &random);
                next_node = (unsigned int)(random * curr_node.size());
                next_node = curr_node[next_node];
            }

            #pragma omp atomic
            rank_list[next_node]++;

            curr_node = adj_list.Edges(next_node);
        }
    }
}