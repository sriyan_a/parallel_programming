#!/bin/bash
echo " " > fb_output.txt
for((i = 1; i <= 8; i = i*2))
do
    # echo "Number of threads: $i" >> fb_output.txt
    for((j = 10; j <= 320; j = j*2))
    do
        # echo "Walk Length: $j" >> fb_output.txt
        ./rank facebook_combined_my.txt $i $j 0.1 >> fb_output.txt
    done
    echo " " >> fb_output.txt
done
