#ifndef PAGERANK_H
#define PAGERANK_H

#include <omp.h>
#include <cstring>
#include "AL.h"

using std::endl;

class PR {
    private:
        static AL adj_list;
        static unsigned int rank_list[1000000];
        static unsigned int walk_len;
        static double damping_ratio;
        static double rank_time;
        static int Rank(void);

    public:
        static int run(const char * filename, unsigned int k, double d);
};

#endif
