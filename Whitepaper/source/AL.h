#ifndef ADJACENCYLIST_H
#define ADJACENCYLIST_H

#include <iostream>
#include <vector>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <ctime>

using std::cin;
using std::cout;
using std::vector;
using std::fstream;
using std::string;
using std::endl;

class AL{
    private:
        vector <vector <unsigned int> > a_list;

    public:
        AL(fstream & adj_steam);
        AL();
        vector <unsigned int> Edges(const unsigned int node) const;
        unsigned int Count(void) const;
};

#endif