#!/bin/bash
./run 1024 8 > output_1024_8.txt
./run 2048 8 > output_2048_8.txt
./run 4096 8 > output_4096_8.txt
./run 8192 8 > output_8192_8.txt
./run 16384 8 > output_16384_8.txt
./run 32768 8 > output_32768_8.txt
./run 65536 8 > output_65536_8.txt
./run 131072 8 > output_131072_8.txt
./run 262144 8 > output_262144_8.txt
./run 524288 8 > output_524288_8.txt
./run 1048576 8 > output_1048576_8.txt
./run 2097152 8 > output_2097152_8.txt
./run 4194304 8 > output_4194304_8.txt
./run 8388608 8 > output_8388608_8.txt
./run 16777216 8 > output_16777216_8.txt
./run 33554432 8 > output_33554432_8.txt
./run 67108864 8 > output_67108864_8.txt
./run 134217728 8 > output_134217728_8.txt
./run 268435456 8 > output_268435456_8.txt
./run 536870912 8 > output_536870912_8.txt
./run 1073741824 8 > output_1073741824_8.txt
