#include "omp.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>

#define RADIUS 0.5
#define MAX_PREC 400

double estimate_pi(int num_cores, long num_darts) {
    omp_set_dynamic(0);
    long long hits = 0, acc = 0;

    #pragma omp parallel num_threads(num_cores)  private(hits) reduction(+:acc)
    {
        long i;
        hits = 0;
        #pragma omp for
        for (i = 0; i < num_darts; i++) {
            int rank = omp_get_thread_num();
			int seed = rank+1;
			seed = seed*i;
            double r1 = ((double)rand_r(&seed)/RAND_MAX);
            seed = seed * time(0);
            double r2 = ((double)rand_r(&seed)/RAND_MAX);

            double ed = ((RADIUS-r1)*(RADIUS-r1))+((RADIUS-r2)*(RADIUS-r2));

            if (ed <= pow(RADIUS, 2))
                hits++;
        }
        acc += hits;
    }

    double pi = 4*((double)acc/(double)num_darts);
    return pi;
}

void main(long argc, char *argv[]) {
    long darts = atol(argv[1]);
    int threads =  atoi(argv[2]);
    double ground_truth_pi = 3.141592653589793238462643383279;
    int n;

    omp_set_num_threads(threads);
    double startTime = omp_get_wtime();
    double pi = estimate_pi(threads, darts);
    double endTime = omp_get_wtime();
    double acc = (pi/ground_truth_pi)*100;
    double error = fabs(100.000 - acc);

    printf("darts: %ld, threads: %d\n", darts, threads);
    printf("Estimated Pi: %.30lf\n", pi);
    printf("Error : %lf percent\n", error);
    printf("Time taken is: %lf\n", 1000*(endTime-startTime));
    printf("---------------------------------------------\n");
}