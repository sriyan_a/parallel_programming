#!/bin/bash

gcc -o run -fopenmp pi_estimator.c -lm
for((op = 1; op <= 8; op = op*2))
do
    echo "#!/bin/bash" > sub.sh
    for((j = 1024; j <= $((2**30)); j = j * 2))
    do
        echo "run ${j} ${op}"
        echo "./run ${j} ${op} > ./outputs/output_${j}_${op}.txt" >> sub.sh
    done
    sbatch -N1 sub.sh
done