#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

int main(int argc, char *argv[]) {
    int rank, size, i;
    int message_size = 1;
    MPI_Status status;
    struct timeval start_time, end_time;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size < 2) {
        printf("Need at least 2 processes.. :( \n");
    }

    else {
        while (message_size <= 1048576) {                                                                  //Binary message of size 1MB

            if (rank == 0) {
                char message[message_size];                                                                //Allocatint memory to get message of message_size
                
                gettimeofday(&start_time, NULL);
                MPI_Send(message, message_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD);                           //send message to node 1
                gettimeofday(&end_time, NULL);

                int time_taken = (end_time.tv_sec * 1000000 + end_time.tv_usec) -
                                (start_time.tv_sec * 1000000 + start_time.tv_usec);

                printf("Send message of size %d, in time = %d us\n", message_size, time_taken);
            }

            if (rank == 1) {
                char receive[message_size];

                gettimeofday(&start_time, NULL);
                MPI_Recv(receive, message_size, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &status);                  //receive message from node 0
                gettimeofday(&end_time, NULL);

                int time_taken = (end_time.tv_sec * 1000000 + end_time.tv_usec) -
                                (start_time.tv_sec * 1000000 + start_time.tv_usec);

                printf("Received message of size %d, in time = %d us\n", message_size, time_taken);
            }
            
            message_size = message_size * 2;                                                                //two-timing with message size
        }
    }

    MPI_Finalize();
    return 0;
}